#!/usr/bin/python3

"""
 contentApp class
 Simple web application for managing content

 Copyright Jesus M. Gonzalez-Barahona, Gregorio Robles 2009-2015
 jgb, grex @ gsyc.es
 TSAI, SAT and SARO subjects (Universidad Rey Juan Carlos)
 October 2009 - March 2015
"""

import webapp

formulario = """
    <br/><br/>
    <form action="" method = "POST"> 
    <input type="text" name="name" value="">
    <input type="submit" value="Enviar">
    </form>
"""

class contentPostApp (webapp.webApp):
    """Simple web application for managing content.

    Content is stored in a dictionary, which is intialized
    with the web content."""

    # Declare and initialize content
    content = {'/': 'Root page',
               '/page': 'A page'
               }

    def parse(self, request):
        """Return the resource name (including /)"""
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = request.split('\r\n\r\n', 1)[1]  # Corto una vez por \r\n\r\n
        return (metodo, recurso, cuerpo)

    def process(self, parsedRequest):
        """Process the relevant elements of the request."""
        (metodo, resourceName, cuerpo) = parsedRequest

        if metodo == "POST":  # Guarda lo introducido en el formulario como nueva entrada
            resourceName = '/' + cuerpo.split('=')[1]
            self.content[resourceName] = cuerpo.split('=')[1]

        print("el cuerpo es:" + cuerpo)

        if resourceName in self.content.keys():
            httpCode = "200 OK"
            if metodo == "POST":
                htmlBody = "<html><body> Encontrado " + self.content[resourceName] \
                           + " para " + resourceName + formulario + "</body></html>"
            else:
                htmlBody = "<html><body>" + self.content[resourceName] \
                        + formulario + "</body></html>"
        else:
            httpCode = "404 Not Found"
            htmlBody = "Not Found"
        return (httpCode, htmlBody)

if __name__ == "__main__":
    testWebApp = contentPostApp("localhost", 1234)